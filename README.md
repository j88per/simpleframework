# README #

Simple PHP Framework.

After testing so many php frameworks, I became disenfranchised over how each one handles application routing.  Essentially each popular framework uses a text file to relate a url path to a php class.  While this works, it's a lot of manually managing routes to classes/actions.  For something small, no big deal. For a large project, with even only a few dozen routes, you start to get overwhelmed with managing routes.


Using php 5.x autoloader, this breaks a simple URL into MVC placeholders like so:
http://my-domain.com/controller/action/parameters

This builds the Controller/Model directly into the code with no need to manage lines of routes right next to your code base.  It derives the paths directly from classes, and can throw custom 404 error pages when a controller or action isn't found or doesn't exist. 

Controller equals the controller class to load in via auto loader, and Action is the method within that class to call.  Default controller/action is defined in settings. 

Easily extensible with your own modules or 3rd party modules.

### Simple PHP Framework###

* Quick summary
* 0.5.2
* painstakingly simple MVC design

### How do I get set up? ###

To install, just clone/unzip/extract the files into your doc-root of choice.  This framework requires a domain name, IP address or localhost just won't work due to parsing of the URL.  Here's a sample vhost file:


```
<VirtualHost *:80>
  ServerName my-site.dev
  DocumentRoot /var/www/my-site.dev/www
  SetEnv APPLICATION_ENV development

  <Directory /var/www/my-site.dev/www>
    Options FollowSymLinks
    AllowOverride All
    Require all granted
  </Directory>
</VirtualHost>
```
* doc root becomes www under the application.  this way protected files which may include passwords are not stored directly in the document root.
* SetEnv is used as a switch in the code for displaying any code errors.  IF it equals development.
* /protected/settings.php contains settings specific to your environment, including database access, as well as site specific settings.
* /tests contains a single php-unit test case as an example.  The framework does support writing and running unit tests.
* requires a custom .htaccess file or rewrites directly in the vhost declaration. These rewrites retrieve style changes directly, and all other requests route through index.php to take advantage of the automatic routing capability.


### database options ###
I've used this with Mysql, MariaDB, CouchDB, and MongoDB without issue

### components ###
built on Twig and php-activerecord (I know, it's not REALLY an ORM, but it was easy).  Along with Twitter/Bootstrap.  (I love bootstrap).

### use ###
honestly, I don't expect anyone to download/use this.  I use it for a few projects internally and it works well.  It's not lightening fast, but it's not horribly slow either.