<?php
/**
 * ActiveRecord Model for Auth table
 *
 * PHP Version 5
 * 
 * This is a Auth file for php ActiveRecord.
 *
 * @category  ORM
 * @package   Auth
 * @author    andrew carlson <andrew@acarlsonphoto.com>
 * @copyright 2013 andrew carlson
 * @license   Common Development and Distribution License (CDDL-1.0)
 * @version   GIT: $Id: 7651b6ff2e8300b02b5e39aafbcce019628992fd $
 * @link      http://google.com
 */

/**
 * ActiveRecord Model for Auth table
 *
 * Handle users table interactions
 *
 * @category ORM
 * @package  Auth
 * @author   Andrew Carlson <andrew@acarlsonphoto.com>
 * @license  Common Development and Distribution License (CDDL-1.0)
 * @link     http://www.google.com
 */
class Auth extends ActiveRecord\Model
{
	/**
	 * required, cannot be blank
	 */
	static $validates_presence_of = array(
		array('username'),
		array('username', 'message' => 'Username cannot be blank')
	);
	
	/**
	 * cannot match other/existing
	 */
	static $validates_uniqueness_of = array(
        array('username'),
        array(array('username'), 'message' => 'Username must be Unique')
    );
}
