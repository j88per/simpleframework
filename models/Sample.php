<?php
/**
 * ActiveRecord Model for Sample table
 *
 * PHP Version 5
 * 
 * This is a sample file for php ActiveRecord.
 *
 * @category  ORM
 * @package   Sample
 * @author    andrew carlson <andrew@acarlsonphoto.com>
 * @copyright 2013 andrew carlson
 * @license   Common Development and Distribution License (CDDL-1.0)
 * @version   GIT: $Id: 7651b6ff2e8300b02b5e39aafbcce019628992fd $
 * @link      http://google.com
 */

/**
 * ActiveRecord Model for Sample table
 *
 * Handle users table interactions
 *
 * @category ORM
 * @package  Sample
 * @author   Andrew Carlson <andrew@acarlsonphoto.com>
 * @license  Common Development and Distribution License (CDDL-1.0)
 * @link     http://www.google.com
 */
class Sample extends ActiveRecord\Model
{
}