<?php

/**
 * A website view class
 *
 * A controller for website actions
 *
 * PHP Version 5
 *
 * LICENSE: This source file is proprietary code
 *
 * @category  Website
 * @package   Website
 * @author    andrew carlson <andrew@acarlsonphoto.com>
 * @copyright 2013 andrew carlson
 * @license   http://opensource.org/licenses/CDDL-1.0
 * @license   Common Development and Distribution License (CDDL-1.0)
 * @version   GIT: $Id:$
 * @link      http://
 */

namespace Website;

/**
 * Website view class
 *
 * Handle all aspects of The default website
 *
 * @category Website
 * @package  Website
 * @author   Andrew Carlson <andrew@acarlsonphoto.com>
 * @license   http://opensource.org/licenses/CDDL-1.0
 * @license   Common Development and Distribution License (CDDL-1.0)
 * @link     http://
 */
class websiteView extends websiteController
{
    /**
     * setup twig
     */
    function __construct()
    {
        $loader = new \Twig_Loader_Filesystem(TEMPLATES);

        if (CACHING) {
            $this -> twig = new \Twig_Environment($loader, array('cache' => 'cache'));
        } else {
            $this -> twig = new \Twig_Environment($loader, array('debug' => true, ));
            
            // enable debugging
            if (ENVIRONMENT == 'development') {
                $this -> twig -> addExtension(new \Twig_Extension_Debug());
            }
            
            // set some core values
            $this -> twig -> addGlobal("AppName", AppName);
            $this -> twig -> addGlobal("siteUrl", AppUrl);
            $this -> twig -> addGlobal("appVersion", VERSION);
        }
    }

    /**
     * render an entire page, with data if available
     */
    function showPage($page, $data = '')
    {
        $path = strtolower(__NAMESPACE__) ."/". $page . ".html.twig";
        $template = $this -> twig -> loadTemplate($path);
        $template -> display(array(
            'appName' => AppName,
            'data' => $data
        ));
    }

    /**
     * render a block of html
     */
    function showBlock($block, $data = '')
    {
        $path = strtolower(__NAMESPACE__) ."/". $block . ".block.twig";
        $template = $this -> twig -> loadTemplate($path);
        $template -> display(array('data' => $data));
    }

}
