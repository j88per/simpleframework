<?php

/**
 * search model
 *
 * the model for search functions
 *
 * PHP Version 5
 *
 * LICENSE: This source file is proprietary code
 *
 * @category  Website
 * @package   Website
 * @author    andrew carlson <andrew@acarlsonphoto.com>
 * @copyright 2013 andrew carlson
 * @license   http://opensource.org/licenses/CDDL-1.0 
 * @license   Common Development and Distribution License (CDDL-1.0)
 * @version   GIT: $Id:$
 * @link      http://
 */

namespace Website;

/**
 * Website model class
 *
 * Website model - receiving data, manipulating data, etc
 *
 * @category  Website
 * @package   Website
 * @author    andrew carlson <andrew@acarlsonphoto.com>
 * @copyright 2013 andrew carlson
 * @license   http://opensource.org/licenses/CDDL-1.0 
 * @license   Common Development and Distribution License (CDDL-1.0)
 * @version   GIT: $Id:$
 * @link      http://
 */
class websiteModel
{
    /**
     * a simple form handler for demo purposes
     */
    function contactForm()
    {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $message = $_POST['message'];
        
        return array('state' => 0, 'message' => '<div>Success</div>');
    }
    
    /**
     * get the text about a particular database
     */
    function getDbText($type)
    {
        $dbTypes = array(
            'mysql' => '<a target="_blank" href="http://us2.php.net/manual/en/book.mysql.php">http://us2.php.net/manual/en/book.mysql.php</a>
            <br />
            If you\'re still interested, you can always resort to pure MySQL commands in your code, but this is not the best approach for a secure application.
        ',
            'pdo' => '<a target="_blank" href="http://php.net/manual/en/book.pdo.php">http://php.net/manual/en/book.pdo.php</a>
            <br />
            PHP PDO is a mature database abstraction layer that really makes working
            with databases a lot easier than just raw database access.
            <br />
            While not as complex as an ORM, it provides a lot of good ideas or best practices and is far more secure
            than raw SQL.
            <br />',
            'orm' => '<a target="_blank" href="http://www.phpactiverecord.org/">http://www.phpactiverecord.org/</a>
            <br />
            PHP ActiveRecord is the initial ORM
            mechanism in place.  It simplifies interacting with the database, by
            making each table an object.
            <br />
            By instantiating each object, you can perform standard CRUD
            actions to complex joins to find specific data.
            <br />
            Installing Doctrine ORM would be fairly straightforward.',
            'nosql' => 'NoSQL databases such as Couch, Mongo, and Cassandra are becoming more and more popular.  
            The simple framework can work with any of them straight up or vi an ORM'
        );

        return $dbTypes[$type];
    }
}
