<?php

/**
 * A website controller class
 *
 * A controller for website actions
 *
 * PHP Version 5
 *
 * LICENSE: This source file is proprietary code
 *
 * @category  Website
 * @package   Website
 * @author    andrew carlson <andrew@acarlsonphoto.com>
 * @copyright 2013 andrew carlson
 * @license   http://opensource.org/licenses/CDDL-1.0 
 * @license   Common Development and Distribution License (CDDL-1.0)
 * @version   GIT: $Id: 7651b6ff2e8300b02b5e39aafbcce019628992fd $
 * @link      http://
 */

namespace Website;

/**
 * Website Controller class
 *
 * Handle all aspects of The default website 
 * this is pretty simple, just display static pages
 *
 * @category Website
 * @package  Website
 * @author   Andrew Carlson <andrew@acarlsonphoto.com>
 * @license   http://opensource.org/licenses/CDDL-1.0 
 * @license   Common Development and Distribution License (CDDL-1.0)
 * @link      http://
 */
class websiteController
{
    /**
     * handle any actions as $2 of url
     * Url breakdown of protocol://domain/controller/action 
     * followed by any and all parameters as either get or post
     */
    function __construct()
    {
        // new method so no longer need to copy/paste
        $utility = new \Utility\utilityController;
        $action = $utility->getAction();
        
        $r = get_class_methods($this); // this is ugly but it works
        if (in_array($action, $r)) {
            $this -> model = new websiteModel;
            $this -> view = new websiteView;
            $this->$action(); 
        } else {
            include 'templates/base/404.html';
            die;
        }
    }
    
    /**
     * since there isn't really a grid, we'll use this for the home page
     * other pages will have/call other views
     */
    function grid()
    {     
        $this->view->showPage('index');
    }
    
    /**
     * show an about us type of page using traditional
     * links
     */
    function links()
    {
        $this->view->showPage('links');
    }
    
    /**
     * for another page, these could be consolidated even further
     * to a single function that gets the page passed in.
     */
    function ajax()
    {
        $this->view->showPage('ajax');
    }
    
    /**
     * show text for modules page
     */
    function modules()
    {
        $this->view->showPage('modules');
    }
    
    /**
     * a simple contact form, using GET, display the form, 
     * using POST, process the results.  This could be two
     * separate functions or a single function.
     */
    function contact()
    {
        if (isset($_POST['state'])) {
            // handle form processing in the model
            $r = $this->model->contactForm();
            echo json_encode($r);   // return a json_encoded response
            exit;
        }
        
        $this->view->showPage('forms');
     
    }
    
    function db()
    {
        // this is custom to parse more parameters outside of $_GET
        $this->url = parse_url($_SERVER['REQUEST_URI']);
        $path = explode("/", $this->url['path']);
        $type = (empty($path[2])) ? DEFAULT_ACTION : $path[3];
        
        $dbText = $this->model->getDbText($type);
        
        $this->view->showBlock('db', array('type' => $type, 'text' => $dbText));
    }
    
    /**
     * show an about us type of page using traditional
     * links
     */
    function databases()
    {
        $this->view->showPage('databases');
    }   
}
    
