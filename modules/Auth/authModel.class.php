<?php

/**
 * auth model
 *
 * the model for auth functions
 *
 * PHP Version 5
 *
 * LICENSE: This source file is proprietary code
 *
 * @category  auth
 * @package   auth
 * @author    andrew carlson <andrew@acarlsonphoto.com>
 * @copyright 2013 andrew carlson
 * @license   http://opensource.org/licenses/CDDL-1.0 
 * @license   Common Development and Distribution License (CDDL-1.0)
 * @version   GIT: $Id:$
 * @link      http://
 */

namespace Auth;

/**
 * auth model class
 *
 * auth model - receiving data, manipulating data, etc
 *
 * @category  auth
 * @package   auth
 * @author    andrew carlson <andrew@acarlsonphoto.com>
 * @copyright 2013 andrew carlson
 * @license   http://opensource.org/licenses/CDDL-1.0 
 * @license   Common Development and Distribution License (CDDL-1.0)
 * @version   GIT: $Id:$
 * @link      http://
 */
class authModel
{

    /**
     * are we authenticated?
     */
    function checkAuth()
    {
        if (isset($_SESSION['loggedin']))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * log user in
     */
    function _login($user)
    {
        $username = $user['username'];
        $pass = sha1($user['password'] . " " . SALT);
        $condition = array("username = $username");
        $auth = \Auth::find_by_username_and_password($username, $pass);

        if ($auth)
        {
            $_SESSION['loggedin'] = 1;
            $_SESSION['userid'] = $auth->id;
            $_SESSION['login'] = date('U');
            $_SESSION['group'] = $auth->user_group;
            $_SESSION['perms'] = $auth->perms;
            echo json_encode(array('state' => 1, 'message' => ''));
            exit;
        }
        else
        {
            // create an html message to display
            $message = "<div class='alert alert-danger'>Username or Password incorrect($pass)</div>";
            echo json_encode(array('state' => 0, 'message' => $message));
            exit;
        }
    }

    /**
     * an example of a function to verify a user's group or permissions.  Good for CMS
     * platforms where certain groups of users can access certain content.
     * 
     * @return int $group - the group id, usually synonous with a group table
     */
    function checkGroup()
    {
        if (isset($_SESSION['group']))
        {
            $group = $_SESSION['group'];
        }
        else
        {
            $username = $_SESSION['username'];
            $auth = \Auth::find_by_username($username);
            $group = $auth->group;
        }

        return $group; // may stick with just number, not sure yet
    }

    /**
     * $user is an array of data defining the user, current auth table
     * schema includes username, password, group, and permissions.
     * 
     * @param array $user - username, password (plaintext), group, permissions
     * @return echo json result data for twig to alert
     */
    function saveUser($user)
    {
        $user['password'] = $this->encPassword($user['password']);
        try {
            $auth = new \Auth($user);
            $auth->save();
            echo json_encode(array('state' => 0, 'messages' => 'Success'));
            exit;
        } catch (exception $e) {
            $message = "Failed to save user, ".$user['username']." ".$e->getMessages();
            echo json_encode(array('state' => 1, 'messages' => $message));
            exit();
        }
    }
    
    /**
     * update an existing user record
     * 
     * @param array $user (username, password(plaintext) group, permissions)
     */
    function updateUser($user)
    {
        if (isset($user['password'])) {
            $user['password'] = $this->encPassword($user['password']);
        }
        
        $userDb = \Auth::find_by_username($user['username']);
        
        foreach($user as $label => $value) {
            $userDb->$label = $value;
        }
        
        $userDb->save();
        echo json_encode(array('state' => 0, 'messages' => 'success'));
    }
    
    /**
     * retrieve a single user based on username
     */
    function getUser($username)
    {
        $select = "auths.id as record, auths.*, groups.*";
        $conditions = array("username like '%$username%'");
        
        $user = \Auth::find(array(
            'select' => $select,
            'conditions' => $conditions,
        ));
        
        return $user;
    }
    
    /**
     * hash a password with salt
     * 
     * @param string $password - the plaintext password
     * @return string the encrypted and salted password string
     */
    function encPassword($password)
    {
        return(sha1($password ." ". SALT));
    }
}
