<?php

/**
 * A auth controller class
 *
 * A controller for auth actions
 *
 * PHP Version 5
 *
 * LICENSE: This source file is proprietary code
 *
 * @category  auth
 * @package   auth
 * @author    andrew carlson <andrew@acarlsonphoto.com>
 * @copyright 2013 andrew carlson
 * @license   http://opensource.org/licenses/CDDL-1.0
 * @license   Common Development and Distribution License (CDDL-1.0)
 * @version   GIT: $Id: 7651b6ff2e8300b02b5e39aafbcce019628992fd $
 * @link      http://
 */

namespace Auth;

/**
 * auth Controller class
 *
 * This class is going to be a bit different, while it will
 * match the structure of other sf classes, it may not behave
 * in exactly the same way
 *
 * Handle all aspects of The default auth
 * this is pretty simple, just display static pages
 *
 * @category auth
 * @package  auth
 * @author   Andrew Carlson <andrew@acarlsonphoto.com>
 * @license   http://opensource.org/licenses/CDDL-1.0
 * @license   Common Development and Distribution License (CDDL-1.0)
 * @link      http://
 */
class authController
{

    /**
     * most auth functions will route directly to the model
     */
    function __construct()
    {
        $utility = new \Utility\utilityController;
        $action = $utility->getAction();

        $r = get_class_methods($this);
        // this is ugly but it works
        if (in_array($action, $r))
        {
            $this->model = new authModel;
            $this->view = new authView;
            $this->$action();
        }
        else
        {
            include 'templates/base/404.html';
            die;
        }
    }

    /**
     * show/handle login
     */
    function login()
    {
        $this->model->_login(array('username' => $_POST['user'], 'password' => $_POST['pass']));
    }

    function logout()
    {
        session_destroy();
        header("Location: /");
        exit;
    }
}
