<?php

/**
 * auth model
 *
 * the model for auth functions
 *
 * PHP Version 5
 *
 * LICENSE: This source file is proprietary code
 *
 * @category  auth
 * @package   auth
 * @author    andrew carlson <andrew@acarlsonphoto.com>
 * @copyright 2013 andrew carlson
 * @license   http://opensource.org/licenses/CDDL-1.0
 * @license   Common Development and Distribution License (CDDL-1.0)
 * @version   GIT: $Id:$
 * @link      http://
 */

namespace Auth;

/**
 * auth model class
 *
 * auth model - receiving data, manipulating data, etc
 *
 * @category  auth
 * @package   auth
 * @author    andrew carlson <andrew@acarlsonphoto.com>
 * @copyright 2013 andrew carlson
 * @license   http://opensource.org/licenses/CDDL-1.0
 * @license   Common Development and Distribution License (CDDL-1.0)
 * @version   GIT: $Id:$
 * @link      http://
 */
class authView extends authController
{

    function __construct()
    {
        $loader = new \Twig_Loader_Filesystem(TEMPLATES);
        if (CACHING)
        {
            $this->twig = new \Twig_Environment($loader,
                    array('cache' => 'cache'));
        }
        else
        {
            $this->twig = new \Twig_Environment($loader, array('debug' => true,));
            $this->twig->addExtension(new \Twig_Extension_Debug());
        }
    }

    /**
     * default view for when we're not authenticated
     */
    function showLogin()
    {
        $template = $this->twig->loadTemplate('auth/index.html.twig');
        $template->display(array('site' => AppName));
    }

}
