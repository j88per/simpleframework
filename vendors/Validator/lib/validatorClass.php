<?php

/**
 * A Validator class
 *
 * The way this class works, by instantiating it.
 *
 * PHP Version 5
 *
 * LICENSE: This source file is proprietary code
 *
 * @category  Validator
 * @package   Validator
 * @author    andrew carlson <andrew@acarlsonphoto.com>
 * @copyright 2013 andrew carlson
 * @license   http://opensource.org/licenses/CDDL-1.0
 * @license   Common Development and Distribution License (CDDL-1.0)
 * @version   GIT: $Id: 7651b6ff2e8300b02b5e39aafbcce019628992fd $
 * @link      http://
 */

namespace Validator;

/**
 * Validator class
 *
 * A server side validation class
 *
 * @category Validator
 * @package  Validator
 * @author   Andrew Carlson <andrew@acarlsonphoto.com>
 * @license   http://opensource.org/licenses/CDDL-1.0
 * @license   Common Development and Distribution License (CDDL-1.0)
 * @link      http://
 */
class validator
{

    public static function isEmpty($data)
    {
        if ('' == $data) {
            return true;
        }
        return false;
    }

    public static function isNotEmpty($data)
    {
        if ('' != $data) {
            return true;
        }

        return false;
    }

    public static function isNumber($data)
    {
        if (ctype_digit($data)) {
            return true;
        }

        return false;
    }

    public static function isAlpha($data)
    {
        if (ctype_alpha($data)) {
            return true;
        }

        return false;
    }

    public static function isAlphaNum($data)
    {
        if (ctype_alnum($data)) {
            return true;
        }

        return false;
    }

    public static function isEmail($data)
    {
        $result = filter_var($data, FILTER_VALIDATE_EMAIL);

        if ($result) {
            return true;
        }
    }

    public static function isUrl($data)
    {
        if (preg_match('/http:\/\//', $data)) {
            $result = filter_var($data, FILTER_VALIDATE_URL);
            if ($result) {
                return true;
            }

        }

        return false;
    }

}
