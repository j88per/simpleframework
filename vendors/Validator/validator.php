<?php

class Validator_Autoloader
{

    function register()
    {
        if (!defined('PHP_VERSION_ID') || PHP_VERSION_ID < 50300)
            die('Simple Validator requires PHP 5.3 or higher');

        $filepath = __DIR__ . "/lib/";

        $files = scandir($filepath);
        // scan all files in folder, drop . and .. (sloppy)
        array_shift($files);
        array_shift($files);

        foreach ($files as $file) {
            if (file_exists($filepath . $file)) {
                $load = $filepath . "$file";
                require_once $load;
            }
        }   
    }

}
