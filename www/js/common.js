/**
 *
 * common javascript functions can be here, or a minified version can be used.
 * I'm not fully embracing bootstrap or node.js yet but probably should.  1 thing at a time.
 *
 * PHP Version 5
 *
 * @category  Website
 * @package   Website
 * @author    andrew carlson <andrew@acarlsonphoto.com>
 * @copyright 2013 andrew carlson
 * @license   http://opensource.org/licenses/CDDL-1.0
 * @license   Common Development and Distribution License (CDDL-1.0)
 * @version   GIT: $Id:$
 * @link      http://
 */
menuSelect = function(fn) {
	var start = Date.now();

	$.get('/website/' + fn, function(data) {
		// clean up content area and append info
		$("#content").empty();
		$("#content").append(data);

		// remove all classes from menu items
		$("#menu ul li").each(function(index) {
			$(this).removeClass('active');
		});

		// add selected class to menu
		$("#" + fn).parent().addClass('active');
	});
}