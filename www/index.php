<?php
/**
 *
 * A job search engine that matches keyword & more
 *
 * PHP Version 5
 *
 * LICENSE: This source file is proprietary code
 *
 * @category  Website
 * @package   1ensemble
 * @author    andrew carlson <andrew@acarlsonphoto.com>
 * @copyright 2013 andrew carlson
 * @license   http://opensource.org/licenses/CDDL-1.0 
 * @license   Common Development and Distribution License (CDDL-1.0)
 * @version   GIT: $Id:$
 * @link      http://
 */
// include config and start app/bootstrap
require_once '../protected/config.php';
include '../protected/bootstrap.php';

// handle session timeout if set
if (SESSIONS) {
    if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 7200)) {
        // last request was more than 30 minutes ago
        session_unset();
        // unset $_SESSION variable for the run-time
        session_destroy();
        // destroy session data in storage
    }

    $_SESSION['LAST_ACTIVITY'] = time();
}

// handle the routing
$url = parse_url($_SERVER['REQUEST_URI']);
$utility = new \Utility\utilityController;
$controller = $utility->getController($url);

$ctl = new $controller;
