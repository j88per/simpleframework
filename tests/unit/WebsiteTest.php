<?php

/**
 * A unit test class for website functions
 *
 * A class to website user functions
 *
 * PHP Version 5
 *
 * LICENSE: This source file is proprietary code
 *
 * @category  UnitTest
 * @package   Users
 * @author    andrew carlson <andrew@acarlsonphoto.com>
 * @copyright 2013 andrew carlson
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   GIT: $Id: 04df1b258cad561cd8acb3a5b0e3c77039ed84d2 $
 * @link      http://google.com
 */

 require_once '../protected/config.php';
 
/**
 * Website Test class
 *
 * Handle all Website tests
 *
 * @category UnitTest 
 * @package  PHPUnit
 * @author   Andrew Carlson <andrew@acarlsonphoto.com>
 * @license  Common Development and Distribution License (CDDL-1.0)
 * @link     http://www.google.com
 */
class websiteTest extends PHPUnit_Framework_TestCase
{
    /**
     * setup for test
     */
    protected function setUp()
    {
    }
    
    /**
     * test each of the web page functions, ensure a page is delivered
     */
    public function testGrid()
    {
        $expected = "<!DOCTYPE html>";
        $_SERVER['REQUEST_URI'] = '/website/';
        
        $this->expectOutputRegex($expected);
        $data = new \Website\websiteController;
    }
}