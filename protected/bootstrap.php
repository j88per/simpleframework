<?php

/* setup autoloading of classes */
/**
 * Autoloader
 *
 * Load and register classes based on namespace designation
 *
 * @category Autoloader
 * @package  Autoloader
 * @author    andrew carlson <andrew@acarlsonphoto.com>
 * @copyright 2013 andrew carlson
 * @license   http://opensource.org/licenses/CDDL-1.0 Common Development and
 * Distribution License (CDDL-1.0)
 * @version   GIT: $Id: 7651b6ff2e8300b02b5e39aafbcce019628992fd $
 * @link      http://
 */

if ('SESSIONS') {
    session_start();
}

/* require additional info for bootstrap */
// @TODO is there anyway to automate this? Having to manually enter a new module
// every time rather defeats the purpose.  This works live, but what about unit testing?
require_once __DIR__ . '/settings.php';

if (USE_TWIG) {
    require_once '../vendors/Twig/lib/Twig/Autoloader.php'; // only needed if you use twig
    
    // instantiate twig
    Twig_Autoloader::register();
}

if (USE_AR) {
    require_once '../vendors/php-activerecord/ActiveRecord.php';

    // setup active record
    ActiveRecord\Config::initialize(function($cfg)
    {
        $cfg -> set_model_directory('../models');
        $cfg -> set_connections(array('development' => 'mysql://' . DBUSER . ':' . DBPASS . '@' . DBHOST . '/' . DBNAME . ';charset=utf8'));
    });
}

// how to add a vendor module
require_once '../vendors/Validator/validator.php';
Validator_Autoloader::register();

/**
 * Simple autoloader
 *
 * @package default
 * @author    andrew carlson <andrew@acarlsonphoto.com>
 */
function __autoload($classname)
{
    $filepath = "" . str_replace('\\', '/', $classname);
    $paths = array(
        '../modules',
        '../vendors'
    );
    $counter = 0;

    foreach ($paths as $path) {
        $pathName = "$path/$filepath.class.php";

        if (file_exists($pathName)) {
            require_once $pathName;
            break;
        } else {
            // we need to cycle through all paths before failing
            if ($counter >= count($paths)) {
                include 'templates/base/404.html';
                die;
            }
        }
        $counter++;
    }
}

spl_autoload_register('__autoload');
