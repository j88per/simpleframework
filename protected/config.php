<?php
/**
 * Config file sets up the autoloader
 *
 * @author    andrew carlson <andrew@acarlsonphoto.com>
 * @copyright 2013 andrew carlson
 * @license   http://opensource.org/licenses/CDDL-1.0 Common Development and Distribution License (CDDL-1.0)
 * @version   GIT: $Id: 7651b6ff2e8300b02b5e39aafbcce019628992fd $
 * @link      http://
 */

/**
 * Setup environment
 *
 * @package default
 * @author  andrew carlson <andrew@acarlsonphoto.com.com>
 */

/* for dev only, comment out for producton */
if (getenv('APPLICATION_ENV') == 'development') {
    ini_set('display_errors', 1);
} else {
    ini_set('display_errors', 0);
}