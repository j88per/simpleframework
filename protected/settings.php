<?php
/**
 * 
 * Application level settings
 *
 * @author    andrew carlson <andrew@acarlsonphoto.com>
 * @copyright 2013 andrew carlson
 * @license   http://opensource.org/licenses/CDDL-1.0 
 * @license   Common Development and Distribution License (CDDL-1.0)
 * @version   GIT: $Id:$
 * @link      http://
 */
 
/**
 * a place to keep code/settings across the platform
 * instead of replacing these values
 */

/* Application Name */
define("AppName", 'simpleFramework');
define("AppUrl", 'http://testdev/');
define("VERSION", "1.0");

/* define run environement (dev/staging/production) */
define("ENVIRONMENT", "development");

/* do we need to use sessions? */
define("SESSIONS", false);
 
/* define database access */
define("DBHOST", 'localhost');
define("DBUSER", 'root');
define("DBPASS", '');
define("DBNAME", 'test');

/* Google Apps settings */
define("CLIENT_ID", '646246491776.apps.googleusercontent.com');
define("CLIENT_SECRET", '-8WCr0aRMnwTTIbdBbrVpU2A');
define("REDIRECT_URI", '');

/* SALT String */
define("SALT", "A Salt string");

// what do we do with requests
define("DEFAULT_ROUTE", "website");

// default action - what we normally do when entering a route initially
define("DEFAULT_ACTION", "grid");

// twig settings
define("USE_TWIG", true);
if (preg_match('/tests/', getcwd())) {
    define("TEMPLATES", '../www/templates');    
} else {
    define("TEMPLATES", 'templates');
}

define("CACHE", $_SERVER['DOCUMENT_ROOT'].'/cache');
define("CACHING", false);

/* active record */
define('USE_AR', true);
